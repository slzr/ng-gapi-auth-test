import { AuthServiceService } from './auth-service.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private _auth: AuthServiceService
  ){
    
  }


  signIn(){
    this._auth.signIn();
  }
}
