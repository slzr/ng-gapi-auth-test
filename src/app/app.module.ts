import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AuthServiceService } from './auth-service.service';

import {
  GoogleApiModule,
  GoogleApiService,
  GoogleAuthService,
  NgGapiClientConfig,
  NG_GAPI_CONFIG,
  GoogleApiConfig
} from "ng-gapi";

let gApiClientConfig: NgGapiClientConfig = {
  client_id: '216348546268-vb8cjcduk54f61cu5lm4nm1p0bic4mhh.apps.googleusercontent.com',
  discoveryDocs: ['https://analyticsreporting.googleapis.com/$discovery/rest?version=v4'],
  scope: [
    "profile", "email",
    "https://www.googleapis.com/auth/adwords"
  ].join(' ')
};


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    GoogleApiModule.forRoot({
      provide  : NG_GAPI_CONFIG,
      useValue : gApiClientConfig
    }),
  ],
  providers: [
    AuthServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
