import { Injectable } from "@angular/core";
import { GoogleAuthService } from "ng-gapi";

@Injectable()
export class AuthServiceService {
  constructor(private gAuth: GoogleAuthService) {
    console.log('mi servicio');
  }

  public signIn(): void {
    this.gAuth.getAuth().subscribe(auth => {
      console.log('auth', auth);

      auth.grantOfflineAccess().then(res => {
        console.log('res', res);
      })
    });
  }

  public signOut(): void{
    this.gAuth.getAuth().subscribe(auth => {
      auth.signOut();
    });
  }

}
